package ua.softgroup.testapplication

import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.FragmentTransaction
import android.transition.TransitionInflater
import kotlinx.android.synthetic.main.activity_main.*
import android.view.MotionEvent
import android.view.View
import android.view.View.OnTouchListener



class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        root.setOnTouchListener(object : OnTouchListener {
            override fun onTouch(v: View, event: MotionEvent): Boolean {
                hideKeyboard()
                return false
            }
        })

        val transaction = supportFragmentManager.beginTransaction()
            .replace(R.id.fragmentContainer, EmailFragment(), EmailFragment::class.java.simpleName)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)

        transaction.commitAllowingStateLoss()



    }
}
