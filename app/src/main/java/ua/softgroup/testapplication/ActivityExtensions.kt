package ua.softgroup.testapplication

import android.content.Context
import android.app.Activity
import android.support.v4.content.ContextCompat.getSystemService
import android.view.inputmethod.InputMethodManager


fun Activity.hideKeyboard() {
    val inputMethodManager = this.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
    if(this.currentFocus !=null) inputMethodManager.hideSoftInputFromWindow(this.currentFocus.windowToken, 0)
}