package ua.softgroup.testapplication


import android.os.Build
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_email_verification.*


class EmailVerificationFragment : Fragment() {

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_email_verification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        btDone.setOnClickListener {
            val transaction = fragmentManager!!.beginTransaction()
                .replace(
                    R.id.fragmentContainer,
                    EmailVerificationDoneFragment(),
                    EmailVerificationDoneFragment::class.java.simpleName
                )
                .addToBackStack(null)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)

            transaction.commitAllowingStateLoss()
        }
    }
}
